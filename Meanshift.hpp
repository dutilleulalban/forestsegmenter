/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <iostream>
#include <functional>

#include "ClusterFactory.hpp"
#include "Approximation.hpp"
#include "Constants.hpp"
#include "Utils.hpp"

template<typename R, size_t Bound, AsTemplateArg<ClustersParameters<R>> Params, typename std::enable_if<std::is_floating_point<R>::value>::type* = nullptr>
class Meanshift
{
public:
    template<typename... Args>
    static std::vector<Cluster<R>> Run(std::vector<Vector3<R>> const& points, const std::function<R(const Vector3<R>&, const Vector3<R>&, Args...)>& kernel, const Args& ...args)
    {
        ClusterFactory<R, Params> factory(points);
        size_t convergenceIteration = 0;

        while (!factory.AllPointsFinal() && convergenceIteration < Bound)
        {
            #pragma omp parallel for\
            shared(points, factory, kernel) \
            schedule(dynamic)

            for (size_t i = 0; i < points.size(); i++)
            {
                if (factory.IsPointFinal(i))
                    continue;    

                const Vector3<R> pointToShift = factory.GetShiftedPointed(i);
                Vector3<R> newPosition;
                R combinedWeight = 0.0;

                for (const auto& point : points)
                {
                    Vector3<R> v = point;

                    const R kernelResult = kernel(point, pointToShift, std::forward<Args>(args)...);
                    
                    newPosition += (v * kernelResult);
                    combinedWeight += kernelResult;
                }

                newPosition /= combinedWeight;

                factory.ShiftPoint(i, newPosition);
            }

            convergenceIteration++;
        }

        return factory.Build();
    }
};
