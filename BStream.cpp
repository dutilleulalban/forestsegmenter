/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "BStream.hpp"
#include <cstring>

namespace IO
{
	BStream::BStream(const std::filesystem::path& path) : readingPosition(0)
	{
        std::ifstream stream(path, std::ifstream::binary);

        if (stream.fail())
            throw std::runtime_error("Failed to open file for BinaryStream");

        stream.seekg(0, std::ios::end);
        const auto wpos = static_cast<size_t>(stream.tellg());
        stream.seekg(0, std::ios::beg);

        buffer.resize(wpos);
        stream.read(reinterpret_cast<char*>(buffer.data()), buffer.size());
	}

    BStream::BStream(std::vector<std::uint8_t>& buffer) : buffer(std::move(buffer)), readingPosition(0) {}

    BStream::BStream(BStream&& other) noexcept : buffer(std::move(other.buffer)), 
                                        readingPosition(other.readingPosition)
    {
        other.readingPosition = 0;
    }

    void BStream::ReadBytes(void* dest, size_t length)
    {
        if (length == 0) return;

        if (readingPosition + length > buffer.size())
            throw std::domain_error("readingPosition + length > buffer.size()");

        std::memcpy(dest, &buffer[readingPosition], length);
        readingPosition += length;
    }


}
