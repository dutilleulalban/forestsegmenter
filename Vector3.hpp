/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <cmath>
#include <string>

#define VECTOR_VEC_EQ_OPERATION(OP, C)                                  \
    C<T>& operator OP##= (C<T> const& o) {							\
        for (size_t i = 0; i < sizeof(raw) / sizeof(raw[0]); ++i)		\
            raw[i] OP##= o[i];											\
        return *this;													\
    }

#define ATOMIC_VEC_EQ_OPERATION(OP, C)                                     \
    C<T>& operator OP##= (T atomic) {									   \
        for (size_t i = 0; i < sizeof(raw) / sizeof(raw[0]); ++i)		   \
            raw[i] OP##= atomic;										   \
        return *this;													   \
    }

#define VECTOR_VEC_OPERATION(OP, C)										\
    C<T> operator OP (C<T> const& o) {								\
        for (size_t i = 0; i < sizeof(raw) / sizeof(raw[0]); ++i)		\
            raw[i] = raw[i] OP o[i];									\
        return *this;													\
    }																	\

#define ATOMIC_VEC_OPERATION(OP, C)											\
    C<T> operator OP (T atomic) {											\
        for (size_t i = 0; i < sizeof(raw) / sizeof(raw[0]); ++i)			\
            raw[i] = raw[i] OP atomic;										\
        return *this;														\
    }																		\

template <typename T, typename std::enable_if<std::is_floating_point<T>::value>::type* = nullptr>
class Vector3 final
{
public:
	static constexpr size_t Dimensions = 3;
	union
	{
		struct
		{
			T x;
			T y;
			T z;
		};

		T raw[Dimensions];
	};

	Vector3() : Vector3(0.0, 0.0, 0.0) { }
	Vector3(T x, T y, T z) : x(x), y(y), z(z) { }

	T& operator [] (size_t idx) { return raw[idx]; }
	T const& operator [] (size_t idx) const { return raw[idx]; }

	constexpr size_t Size() { return Dimensions; }

	T Distance(const Vector3& o) const
	{
		T dx = x - o.x;
		T dy = y - o.y;
		T dz = z - o.z;

		return std::sqrt(dx * dx + dy * dy + dz * dz);
	}

	T Length() const
	{
		return std::sqrt(LengthSquared());
	}

	T LengthSquared() const
	{
		return x * x + y * y + z * z;
	}

	Vector3<T> Cross(Vector3<T> const& o) const
	{
		T x_ = y * o.z - o.y * z;
		T y_ = z * o.x - o.z * x;
		T z_ = x * o.y - o.x * y;
		return Vector3<T> {x_, y_, z_};
	}

	Vector3<T> Norm() const
	{
		T l = Length();
		return Vector3<T>{ x / l, y / l, z / l };
	}

	bool operator==(Vector3 const& o) { return x == o.x && y == o.y && z == o.z; }
	bool operator!=(Vector3 const& o) { return !(*this == o); };

	bool operator>=(Vector3 const& o) { return ((x >= o.x) && (y >= o.y) && (z >= o.z)); };
	bool operator<=(Vector3 const& o) { return ((x <= o.x) && (y <= o.y) && (z <= o.z)); };
	bool operator>(Vector3 const& o) { return !(*this <= o); };
	bool operator<(Vector3 const& o) { return !(*this >= o); };

	VECTOR_VEC_EQ_OPERATION(+, Vector3);
	VECTOR_VEC_EQ_OPERATION(-, Vector3);
	VECTOR_VEC_EQ_OPERATION(*, Vector3);
	VECTOR_VEC_EQ_OPERATION(/ , Vector3);

	ATOMIC_VEC_EQ_OPERATION(+, Vector3);
	ATOMIC_VEC_EQ_OPERATION(-, Vector3);
	ATOMIC_VEC_EQ_OPERATION(*, Vector3);
	ATOMIC_VEC_EQ_OPERATION(/ , Vector3);

	VECTOR_VEC_OPERATION(+, Vector3);
	VECTOR_VEC_OPERATION(-, Vector3);
	VECTOR_VEC_OPERATION(*, Vector3);
	VECTOR_VEC_OPERATION(/ , Vector3);

	ATOMIC_VEC_OPERATION(+, Vector3);
	ATOMIC_VEC_OPERATION(-, Vector3);
	ATOMIC_VEC_OPERATION(*, Vector3);
	ATOMIC_VEC_OPERATION(/ , Vector3);
};

#undef VECTOR_VEC_EQ_OPERATION
#undef ATOMIC_VEC_EQ_OPERATION
#undef VECTOR_VEC_OPERATION
#undef ATOMIC_VEC_OPERATION