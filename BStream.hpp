/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <filesystem>
#include <ostream>
#include <fstream>
#include <vector>
#include <type_traits>

namespace IO
{
	class BStream
	{
		public:
			BStream(const std::filesystem::path& path);
			BStream(std::vector<std::uint8_t>& buffer);
			BStream(BStream&& other) noexcept;

			template <typename T>
			T Read()
			{
				T ret;
				Read(ret);
				return ret;
			}

			template <typename T> 
			void Read(T& out)
			{
				ReadBytes(&out, sizeof(T));
			}

			void ReadBytes(void* dest, size_t length);

			const size_t GetPosition() { return readingPosition; }
			void SetPosition(size_t pos) { readingPosition = pos; }
		private:
			size_t readingPosition;
			std::vector<uint8_t> buffer;
	};

	template <typename T>
	BStream& operator >> (BStream& stream, T& data)
	{
		stream.Read(data);
		return stream;
	}
}