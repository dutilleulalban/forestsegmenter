/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "CoordinatesFile.hpp"
#include "Meanshift.hpp"
#include "Utils.hpp"

#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <ostream>
#include <numeric>
#include <filesystem>
#include <chrono>

enum class Coords
{
    X,
    Y,
    Z
};

std::pair<Vector3<PComp>, Vector3<PComp>> GetMinMaxCoordinatesAxis(std::vector<Vector3<PComp>> const& points, Coords c)
{
    std::pair< Vector3<PComp>, Vector3<PComp>> coords;;
    const auto lX = [](Vector3<PComp> const& lhs, Vector3<PComp> const& rhs) {return lhs.x < rhs.x; };
    const auto lY = [](Vector3<PComp> const& lhs, Vector3<PComp> const& rhs) {return lhs.y < rhs.y; };
    const auto lZ = [](Vector3<PComp> const& lhs, Vector3<PComp> const& rhs) {return lhs.z < rhs.z; };

    switch (c)
    {
    case Coords::X:
        coords.first = *std::min_element(std::begin(points), std::end(points), lX);
        coords.second = *std::max_element(std::begin(points), std::end(points), lX);
        break;
    case Coords::Y:
        coords.first = *std::min_element(std::begin(points), std::end(points), lY);
        coords.second = *std::max_element(std::begin(points), std::end(points), lY);
        break;
    case Coords::Z:
        coords.first = *std::min_element(std::begin(points), std::end(points), lZ);
        coords.second = *std::max_element(std::begin(points), std::end(points), lZ);
        break;
    }

    return coords;
}

template <typename T, typename std::enable_if<std::is_floating_point<T>::value>::type* = nullptr>
struct MeanshiftParameters
{
public:
    const T CylinderRadius;
    const T StandardDeviation;
    const T VerticalBandwidthD2;
    const T VerticalBandwidthD4;
    const T VerticalBandwithNormalization;
};

int main(int argc, char** argv)
{
    if (argc < 4)
    {
        std::printf("Usage : %s [inputFile] [outputFile] [horizontalBandwidth] [verticalBandwidth] \n", argv[0]);
        return EXIT_FAILURE;
    }

    const std::string inputFile(argv[1]);
    const std::string outputFile(argv[2]);
    const PComp horizontalBandwidth = static_cast<PComp>(std::stod(argv[3]));
    const PComp verticalBandwidth = static_cast<PComp>(std::stod(argv[4]));

    std::vector<Vector3<PComp>> cloudPoints;

    if (!std::filesystem::exists(inputFile))
    {
        std::printf("[-] File %s not found, aborting ! \n", inputFile.c_str());
        return EXIT_FAILURE;
    }

    if (!IO::Coordinates::File::Deserialize<PComp>(inputFile, cloudPoints))
    {
        std::printf("[-] Could not deserialize file %s, aborting ! \n", inputFile.c_str());
        return EXIT_FAILURE;
    }

    // We don't want to work on a too large sample, so let's use a smaller area

    cloudPoints.erase(std::remove_if(cloudPoints.begin(), cloudPoints.end(),
        [](Vector3<PComp> vec)
        { return (vec.x < Constants::Area::AreaMinX || vec.x > Constants::Area::AreaMaxX) || (vec.y < Constants::Area::AreaMinY || vec.y > Constants::Area::AreaMaxY); }), cloudPoints.end());

    const auto kernel = [](const Vector3<PComp>& point, const Vector3<PComp>& pointToShift,
                           const MeanshiftParameters<PComp>& params) -> PComp
    {
        // Euclidian distance computed only for the gaussian density

        const PComp distance = pointToShift.Distance(point);

        PComp kernelResult = 0.0;
        PComp gaussianDensity = 0.0;

        if (distance <= params.CylinderRadius) {

            // Bounds for the mask that will be used (see below)

            const PComp minFactorEpanechnikov = pointToShift.z - (params.VerticalBandwidthD4);
            const PComp maxFactorEpanechnikov = pointToShift.z + (params.VerticalBandwidthD2);

            const bool heightFilter = point.z <= maxFactorEpanechnikov && minFactorEpanechnikov <= point.z;

            // We want to give more importance to the points at the top of the kernel, thus we use a mask

            const PComp heightDist =
                (heightFilter)
                ? std::min
                (
                    std::abs((((minFactorEpanechnikov)-point.z) / (params.VerticalBandwithNormalization))),
                    std::abs((((maxFactorEpanechnikov)-point.z) / (params.VerticalBandwithNormalization)))
                )
                : 0.0;

            const PComp epanechnikovVertical = heightFilter ? 1 - (std::abs(1 - heightDist) * std::abs(1 - heightDist)) : 0.0;

            // Spline approximation on the exponential function which has no dedicated instructions (at least on x86)

            if constexpr (Constants::MeanShift::UseApproximations)
                gaussianDensity = Approximation::Exp(-(distance * distance) / params.StandardDeviation);
            else
                gaussianDensity = std::exp(-(distance * distance) / params.StandardDeviation);

            // Combine the two inner kernels (Gaussian and Epanechnikov)
            kernelResult = (gaussianDensity * epanechnikovVertical);
        }

        return kernelResult;
    };

    const MeanshiftParameters<PComp> params =
    {
        .CylinderRadius = static_cast<PComp>(horizontalBandwidth * 3.),
        .StandardDeviation = static_cast<PComp>(2. * (horizontalBandwidth * horizontalBandwidth)),
        .VerticalBandwidthD2 = verticalBandwidth / 2,
        .VerticalBandwidthD4 = verticalBandwidth / 4,
        .VerticalBandwithNormalization = static_cast<PComp>((3. * verticalBandwidth) / 8.)
    };

    constexpr ClustersParameters<PComp> clusterParams(Constants::MeanShift::ClusterEpsilon, Constants::Cluster::ShiftingFactor);

    std::printf("[+] Starting to clusterize %zu points\n", cloudPoints.size());

    const auto t1 = std::chrono::high_resolution_clock::now();
    const auto clusters = Meanshift<PComp, 150, clusterParams>::Run<const MeanshiftParameters<PComp>&>(cloudPoints, std::function<PComp(const Vector3<PComp>&, const Vector3<PComp>&, const MeanshiftParameters<PComp>&)>(kernel), params);
    const auto t2 = std::chrono::high_resolution_clock::now();
    const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();

    std::printf("[+] Created %zu clusters in %ld milliseconds ... \n", clusters.size(), duration);

    size_t i = 0;

    std::ofstream outCluster;
    outCluster.open(outputFile);

    for (const auto& cluster : clusters)
    {
        const auto centroid = cluster.Centroid();

        const auto minMaxZ = GetMinMaxCoordinatesAxis(cluster.pts, Coords::Z);
        const auto minMaxY = GetMinMaxCoordinatesAxis(cluster.pts, Coords::Y);
        const auto minMaxX = GetMinMaxCoordinatesAxis(cluster.pts, Coords::X);

        PComp diffZ = minMaxZ.second.z - minMaxZ.first.z;
        PComp diffXY = std::max(std::abs(minMaxY.second.x - minMaxY.first.x),
                                 std::abs(minMaxX.second.y - minMaxX.first.y)
                                );

        const bool isBadCluster = diffZ >= Constants::Decimate::HeightThreshold && diffXY <= Constants::Decimate::RadiusThreshold;

        std::printf("[~] Cluster(%zu) of size %zu with diff(x, z) = (%lf,%lf) -> %d \n", i, cluster.pts.size(), diffXY, diffZ, static_cast<int32_t>(isBadCluster));

        for (const auto& point : cluster.pts)
            outCluster << StringFormat("%lf,%lf,%lf,%d,%zu", point.x, point.y, point.z, static_cast<int32_t>(isBadCluster), i) << std::endl;

        i++;
    }

    outCluster.close();

    std::printf("[+] Clusters saved in %s \n", outputFile.c_str());

    return EXIT_SUCCESS;
}