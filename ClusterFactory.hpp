/*
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include "Cluster.hpp"
#include "Constants.hpp"
#include <algorithm>

template<typename R, typename std::enable_if<std::is_floating_point<R>::value>::type* = nullptr>
class ClustersParameters
{
public:
	constexpr ClustersParameters(R epsilon, R factor) : ClusterEpsilon(epsilon), ShiftingFactor(factor) {}
	constexpr ClustersParameters() : ClusterEpsilon(0.0), ShiftingFactor(0.0) {}

    R ClusterEpsilon;
    R ShiftingFactor;
};

template<typename R, ClustersParameters<R> Params, typename std::enable_if<std::is_floating_point<R>::value>::type* = nullptr>
class ClusterFactory final
{
public: 
	ClusterFactory(std::vector<Vector3<R>> const& originalPoints)
	{
		this->originalPoints = originalPoints;
		this->shiftedPoints = originalPoints;
		this->clusterEpsilon = Params.ClusterEpsilon;
		this->shiftingEpsilon = Params.ClusterEpsilon / Params.ShiftingFactor;
		this->shiftingPoints = std::vector<bool>(originalPoints.size(), true);
	}

	bool IsPointFinal(const size_t i) { return !shiftingPoints[i]; }
	Vector3<R>& GetShiftedPointed(const size_t i) { return shiftedPoints[i]; }

	bool AllPointsFinal() { return std::none_of(shiftingPoints.begin(), shiftingPoints.end(), [] (bool v) { return v; }); }

	void ShiftPoint(size_t index, Vector3<R> const& pos) 
	{
		if (pos.Distance(shiftedPoints[index]) <= shiftingEpsilon)
			shiftingPoints[index] = false;
		else
			shiftedPoints[index] = pos;
	}

	std::vector<Cluster<R>> Build()
	{
		std::vector<Cluster<R>> clusters;

		for (size_t i = 0; i < shiftedPoints.size(); ++i) {
			Vector3<R> shiftedPoint = shiftedPoints[i];

			auto cluster = clusters.begin();
			auto lastCluster = clusters.end();

			while (cluster != lastCluster) {
				if (cluster->Centroid().Distance(shiftedPoint) <= clusterEpsilon) {
					cluster->Add(originalPoints[i]);
					break;
				}
				++cluster;
			}
			if (cluster == lastCluster) {
				Cluster<R> cluster(shiftedPoint);
				cluster.Add(originalPoints[i]);
				clusters.emplace_back(cluster);
			}
		}

		return clusters;
	}

private:
	std::vector<Vector3<R>> originalPoints;
	std::vector<Vector3<R>> shiftedPoints;
	std::vector<bool> shiftingPoints;

	R clusterEpsilon = 0.0;
	R shiftingEpsilon = 0.0;
};