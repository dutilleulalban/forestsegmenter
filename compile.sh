export PATH=/opt/gcc-latest/bin:$PATH
export LD_RUN_PATH=/opt/gcc-latest/lib64

g++  -std=c++2a FastSegmenter.cpp BStream.cpp -o ./bin/segmenter -O3 -fopenmp -ffast-math -march=native -fomit-frame-pointer
