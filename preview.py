'''
Copyright (c) 2021 Alban Dutilleul

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

import os
import plotly.graph_objects as go
import argparse
import numpy as np


parser = argparse.ArgumentParser(description='Display a 3D plot showing clusters inside a cloud point.')
parser.add_argument('file', type=str,
                    help='A CSV File containing the clusters')
parser.add_argument('--decimate', dest='decimate', action='store_true')
parser.add_argument('--no-decimate', dest='decimate', action='store_false')
parser.set_defaults(decimate=True)

def main():
    args = parser.parse_args()
    data = np.genfromtxt(args.file, delimiter=',')
    n_clusters = int(np.max(data[:,-1] + 1))
    clusters = np.ndarray(shape=n_clusters, dtype=np.ndarray)

    for i in range(0, n_clusters):
        clusters[i] = np.float32([point[:-1] for point in data if point[-1] == i])

    idx = 0
    fig = go.Figure()

    for cluster in clusters:
        is_inside_tree = not np.all((cluster[:,3] == 0))
        if not (args.decimate and is_inside_tree):
            fig.add_trace(go.Scatter3d(x = cluster[:,0],
                                    y = cluster[:,1],
                                    z = cluster[:,2],
                                    mode = 'markers', marker_size = 3, marker_line_width = 1,
                                    name = 'Cluster ' + str(idx)))
        idx += 1

    fig.show()


   

if __name__ == '__main__':
    main()
