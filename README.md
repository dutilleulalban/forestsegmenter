# Introduction

This project allows the separation of vegetation within a LiDAR  point cloud in a wooded area. 

It is based on a MeanShift algorithm parallelized with OpenMP in C++ 20. The proposed kernel allows to take into account the density and height maxima of the vegetation to separate it. The approach used avoids making assumptions even if some constants can be changed to obtain better results depending on the type of forest (boreal, tropical, ...).

# Install

## Requirements

1. GCC 11
2. OpenMP (libomp-dev)

```
sudo mkdir ./bin
g++ -std=c++2a FastSegmenter.cpp BStream.cpp -o ./bin/segmenter -O3 -fopenmp -ffast-math -march=native -fomit-frame-pointer
```

## Comments
It should be noted that it is possible to disable OpenMP to obtain a single-threaded version (remove -fopenmp), but this is not recommended due to the poor performance.

The other flags are also there for optimisation, however they can influence the results, for instance -ffast-math.

# Copyright

**License** : MIT

# Usage

```
Usage : ./bin/segmenter [inputFile] [outputFile] [horizontalBandwidth] [verticalBandwidth]
```

The program expects an input file containing a LiDAR  point cloud in the XYZ0 format (see file CoordinatesFile.hpp), so you may have to convert your data. The bandwidth parameters to be provided depend on your sample and are very important, for the moment an empirical approach is suggested although a bandwidth estimator could be implemented.


It is possible to visualise the resulting segmentation using the python executable (version >= 3.8) which requires plotly.

```
pip install plotly
python preview.py out.csv [--decimate]
```

## An example

[<img src="https://i.imgur.com/OF2Nfon.png">]()